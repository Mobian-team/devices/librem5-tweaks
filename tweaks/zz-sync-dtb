#!/bin/sh -e

## Copyright 2019-2020 Vagrant Cascadian <vagrant@debian.org>

## This program comes with ABSOLUTELY NO WARRANTY; for details see
## COPYING.  This is free software, and you are welcome to
## redistribute it under the terms of the GNU GPL, version 2 or any
## later version; see COPYING for details.

# sync .dtb files into /boot to simplify installations on systems with
# a split /boot partition.  To use, install this file into
# /etc/kernel/postinst.d/zz-sync-dtb and mark it as executable.

version="$1"

# passing the kernel version is required
if [ -z "${version}" ]; then
    echo >&2 "W: sync-dtb: ${DPKG_MAINTSCRIPT_PACKAGE:-kernel package} did not pass a version number"
    exit 2
fi

destdir="dtb-${version}"

# HACK: for now u-boot can't handle compressed images, so use boot script
rm -f /boot/dtb /boot/initrd.img /boot/Image.gz

ln -s $version /boot/dtb
if [ ! -e "/boot/$version" -o -h "/boot/$version" ]; then
    rm -f /boot/$version
    ln -s $destdir /boot/$version
fi

ln -s initrd.img-$version /boot/initrd.img

ln -s Image.gz-$version /boot/Image.gz
if [ ! -e "/boot/Image.gz-$version" -o -h "/boot/Image.gz-$version" ]; then
    rm -f /boot/Image.gz-$version
    ln -s vmlinuz-$version /boot/Image.gz-$version
fi
