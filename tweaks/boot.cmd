setenv bootargs console=ttymxc3,115200 consoleblank=0 loglevel=7 root=/dev/mmcblk0p2 rw splash plymouth.ignore-serial-consoles vt.global_cursor_default=0

echo "Loading kernel..."
load ${devtype} ${devnum}:1 ${ramdisk_addr_r} /Image.gz

echo "Uncompressing kernel..."
unzip ${ramdisk_addr_r} ${kernel_addr_r}

echo "Loading initramfs..."
load ${devtype} ${devnum}:1 ${ramdisk_addr_r} /initrd.img
setenv ramdisk_size ${filesize}

echo "Loading dtb..."
load ${devtype} ${devnum}:1 ${fdt_addr_r} /dtb/freescale/imx8mq-librem5-r4.dtb

echo "Booting..."
booti ${kernel_addr_r} ${ramdisk_addr_r}:0x${ramdisk_size} ${fdt_addr_r}
